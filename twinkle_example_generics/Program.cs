﻿// See https://aka.ms/new-console-template for more information
using System;
namespace genericsDemo
{
    public class SomeClass
    {
        public void GenericMethod<T>(T Param1, T Param2)
        {
            Console.WriteLine($"Parameter 1:{Param1} : Parameter 2: {Param2}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generics Method Example");
            SomeClass s=new SomeClass();
            s.GenericMethod<int>(10, 20);
            s.GenericMethod(10.5, 20);
            s.GenericMethod("hi", "there");
            Console.ReadLine();
        }
    }
}