﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");


public class Sports
{
    int sportsid = 0;
    string sportsname = string.Empty;
    string sportstype = string.Empty;

    public int S_Id
    {
        get { return sportsid; }
        set { sportsid = value; }

    }
    public string Sports_Name
    {
        get
        {
            return sportsname;
        }
        set
        {
            sportsname = value;
        }
    }

    public string Sports_Type
    {
        get
        {
            return sportstype;
        }
        set
        {
            sportstype = value;
        }
    }



    public Sports(int sportsid, string sportsname, string sportstype)
    {
        this.sportsname = sportsname;
        this.sportstype = sportstype;
        this.sportsid = sportsid;
    }
}