﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

public class Medicine
{
    int medicineid = 0;
    string medicinename = string.Empty;
    string medicinetype = string.Empty;

    public int M_Id
    {
        get { return medicineid; }
        set { medicineid = value; }

    }
    public string Medicine_Name
    {
        get
        {
            return medicinename;
        }
        set
        {
            medicinename = value;
        }
    }

    public string Medicine_Type
    {
        get
        {
            return medicinetype;
        }
        set
        {
            medicinetype = value;
        }
    }



    public Medicine(int medicineid, string medicinename, string medicinetype)
    {
        this.medicinename = medicinename;
        this.medicinetype = medicinetype;
        this.medicineid = medicineid;
    }
}