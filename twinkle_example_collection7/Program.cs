﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

public class School
{
    int schid = 0;
    string schname = string.Empty;
    string schtype = string.Empty;

    public int S_Id
    {
        get { return schid; }
        set { schid = value; }

    }
    public string School_Name
    {
        get
        {
            return schname;
        }
        set
        {
            schname = value;
        }
    }

    public string School_Type
    {
        get
        {
            return schtype;
        }
        set
        {
            schtype = value;
        }
    }



    public School(int schid, string schname, string schtype)
    {
        this.schname = schname;
        this.schtype = schtype;
        this.schid = schid;
    }
}