﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using twinkle_example_studentdetailsassignment.Models;

namespace twinkle_example_studentdetailsassignment.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}
        private static IList<Student> student = new List<Student>
        {
            new Student{StudentId=Guid.NewGuid(),StudentName="Twinkle",StudentLName="khanna",Studentemail="twinkle@gmail.com",Studentdob="07/03/2000",Studentdoj="07/03/22",Studentstream="cse",Section="sec1",Grade="a",Gender="Female"},
            new Student{StudentId=Guid.NewGuid(),StudentName="Anmol",StudentLName="khanna",Studentemail="anmol@gmail.com",Studentdob="30/07/1997",Studentdoj="30/07/19",Studentstream="cse",Section="sec2",Grade="a+",Gender="Male"},
            new Student{StudentId=Guid.NewGuid(),StudentName="Yash",StudentLName="khanna",Studentemail="yash@gmail.com",Studentdob="03/11/1999",Studentdoj="03/11/22",Studentstream="ee",Section="sec3",Grade="a",Gender="Male"},
            new Student{StudentId=Guid.NewGuid(),StudentName="Prasiddhi",StudentLName="khanna",Studentemail="prasiddhi@gmail.com",Studentdob="17/07/1997",Studentdoj="17/07/18",Studentstream="mechanical",Section="sec4",Grade="a+",Gender="Female"},
            new Student{StudentId=Guid.NewGuid(),StudentName="Garima",StudentLName="khanna",Studentemail="garima@gmail.com",Studentdob="14/11/1991",Studentdoj="14/11/14",Studentstream="ee",Section="sec5",Grade="a",Gender="Female"},
             new Student{StudentId=Guid.NewGuid(),StudentName="Rishi",StudentLName="tandon",Studentemail="rishi@gmail.com",Studentdob="05/02/1993",Studentdoj="05/02/15",Studentstream="IT",Section="sec1",Grade="a+",Gender="Male"},
              new Student{StudentId=Guid.NewGuid(),StudentName="Naman",StudentLName="Mehra",Studentemail="naman@gmail.com",Studentdob="06/01/1990",Studentdoj="06/01/12",Studentstream="civil",Section="sec2",Grade="a",Gender="Male"},
               new Student{StudentId=Guid.NewGuid(),StudentName="Harshita",StudentLName="Mehra",Studentemail="harshita@gmail.com",Studentdob="25/01/1997",Studentdoj="25/0118/",Studentstream="IT",Section="sec3",Grade="b",Gender="Female"},
                new Student{StudentId=Guid.NewGuid(),StudentName="Ritika",StudentLName="Tandon",Studentemail="ritika@gmail.com",Studentdob="06/10/1999",Studentdoj="06/10/21",Studentstream="civil",Section="sec4",Grade="b",Gender="Female"},
                new Student{StudentId=Guid.NewGuid(),StudentName="Dhruv",StudentLName="Tandon",Studentemail="dhruv@gmail.com",Studentdob="29/07/2003",Studentdoj="29/07/25",Studentstream="mechanical",Section="sec2",Grade="c",Gender="Male"}
        };
        public IActionResult Index()
        {
            ViewData["abc"] = "this is an example";
            ViewBag.someexample = "this is another example";
            return View();
        }
        public IActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddStudent(Student input)
        {
            student.Add(new Student { StudentId = Guid.NewGuid(), StudentName = input.StudentName, StudentLName=input.StudentLName, Studentemail=input.Studentemail, Studentdob=input.Studentdob, Studentdoj=input.Studentdoj, Studentstream=input.Studentstream, Section=input.Section, Grade=input.Grade,Gender=input.Gender });
            //return View();
            return RedirectToAction("StudentList");
        }
        [HttpPost]
        public IActionResult Delete(Guid id)
        {
            student.Remove(student.SingleOrDefault(o => o.StudentId == id));
            return RedirectToAction("StudentList");
        }
        public ViewResult Details()
        {
            Student student = new Student()
            {
                StudentId = Guid.NewGuid(),
                StudentName = "Twinkle",
                Section = "section:E",
                Grade = "A",
                Gender = "Female"
            };
            ViewData["Student"] = student;
            return View();
        }

        public ViewResult displayStudents()
        {
            ViewBag.students = student;
            return View();
        }

        public IActionResult StudentList()
        {
            return View(student);
        }
        public IActionResult Privacy()
        {
            return View();
        }



         
         [HttpGet]
         public IActionResult modifyStudent(Guid id)
         {
            var stud = student.SingleOrDefault(o => o.StudentId == id);
            return View(stud); 
        }

        [HttpPost]
        public IActionResult modifyStudent(Student stud)
        {
            student.Remove(student.SingleOrDefault(x => x.StudentId == stud.StudentId));
            student.Add(new Student()
            {
                StudentId = Guid.NewGuid(),
                StudentName = stud.StudentName,
                StudentLName = stud.StudentLName,
                Studentemail = stud.Studentemail,
                Studentdob = stud.Studentdob,
                Studentdoj = stud.Studentdoj,
                Studentstream = stud.Studentstream,
                Section = stud.Section,
                Grade = stud.Grade,
                Gender = stud.Gender,
            });
            return RedirectToAction("StudentList");
        }

        

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}