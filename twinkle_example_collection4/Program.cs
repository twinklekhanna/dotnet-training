﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
public class Department
{
    int deptid = 0;
    string deptname = string.Empty;
    string depttype = string.Empty;

    public int D_Id
    {
        get { return deptid; }
        set { deptid = value; }

    }
    public string Dept_Name
    {
        get
        {
            return deptname;
        }
        set
        {
            deptname = value;
        }
    }

    public string dept_Type
    {
        get
        {
            return depttype;
        }
        set
        {
            depttype = value;
        }
    }



    public Department(int deptid, string deptname, string depttype)
    {
        this.deptname = deptname;
        this.depttype = depttype;
        this.deptid = deptid;
    }
}
