﻿// See https://aka.ms/new-console-template for more information
class program
{
    abstract class AreaClass
    {

        abstract public int Area();

    }

    class Square : AreaClass
    {
        int side = 0;
        //constructor
        public Square(int n)
        {
            side = n;

        }
        //overridden
        public override int Area()
        {
            return side * side;
        }

    }
    static void Main(string[] args)
    {
        Square s = new Square(6);
        Console.WriteLine("Area =" + s.Area());
    }
}
//..............................................................................................................................


