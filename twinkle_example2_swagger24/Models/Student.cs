﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace twinkle_example2_swagger24.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string Roll { get; set; }
       

    }
}
