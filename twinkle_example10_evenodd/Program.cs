﻿using System;

namespace twinkle_example10_evenodd
{
    class Program
    {
        static void Main(string[] args)
        {

            int chknum = 0;
            Console.WriteLine("Enter any number to check if it is even or odd");
            chknum = Convert.ToInt32(Console.ReadLine());
            if(chknum %2==0)
            {
                Console.WriteLine("even number");
            }
            else
            {
                Console.WriteLine("odd number");
            }

        }
    }
}
