﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

public class Employee
{
    int empid = 0;
    string empname = string.Empty;
    string empadd = string.Empty;

    public int E_Id
    {
        get { return empid; }
        set { empid = value; }
    }

    public string Emp_Name
    {
        get
        {
            return empname;
        }
        set
        {
            empname = value;
        }
    }

    public string Emp_Add
    {
        get
        {
            return empadd;
        }
        set
        {
            empadd = value;
        }
    }



    public Employee(int empid, string empname, string empadd)
    {
        this.empname = empname;
        this.empadd = empadd;
        this.empid = empid;
    }
}
