﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

public class Zoo
{
    int cageid = 0;
    string animalname = string.Empty;
    string anmimaltype = string.Empty;

    public int C_Id
    {
        get { return cageid; }
        set { cageid = value; }

    }
    public string Animal_Name
    {
        get
        {
            return animalname;
        }
        set
        {
            animalname = value;
        }
    }

    public string Animal_Type
    {
        get
        {
            return anmimaltype;
        }
        set
        {
            anmimaltype = value;
        }
    }



    public Zoo(int cageid, string animalname, string anmimaltype)
    {
        this.animalname = animalname;
        this.anmimaltype = anmimaltype;
        this.cageid = cageid;
    }
}