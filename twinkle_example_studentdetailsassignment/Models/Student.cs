﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace twinkle_example_studentdetailsassignment.Models
{
    public class Student
    {

        public Guid StudentId { get; set; }
        public string StudentName { get; set; }
        public string StudentLName { get; set; }
        public string Studentemail { get; set; }
        public string Studentdob { get; set; }
        public string Studentdoj { get; set; }

        public string Studentstream { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
        public string Gender { get; set; }
    }
}
