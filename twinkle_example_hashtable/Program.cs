﻿// See https://aka.ms/new-console-template for more information
using System;
using System.Collections;
namespace twinkle_example_hashtable{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();

            ht.Add("001", "twinkle khanna");
            ht.Add("002", "yash khanna");
            ht.Add("003", "anmol khanna");
            ht.Add("004", "prasiddhi khanna");
            ht.Add("005", "garima khanna");
            ht.Add("006", "bela khanna");
            ht.Add("007", "sandeep khanna");

            if (ht.ContainsValue("jyoti khanna"))
            {
                Console.WriteLine("This student name is already in the list");
            }
            else
            {
                ht.Add("008", "jyoti khanna");
               
            }

            // Get a collection of the keys.
            ICollection key = ht.Keys;

            foreach (string k in key)
            {
                Console.WriteLine(k + ": " + ht[k]);
            }
            Console.ReadKey();
        }
    }


}