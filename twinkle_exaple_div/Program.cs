﻿// See https://aka.ms/new-console-template for more information
using System;

namespace twinkle_eample_div
{
    public class Program
    {

        static void Main(string[] args)
        {
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Division - " + (a / b));
        }
    }
}