﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");


public class Student
{
    int studentid = 0;
    string studentname = string.Empty;
    string studentadd= string.Empty;

    public int S_Id
    {
        get { return studentid; }
        set { studentid = value; }

    }
    public string Student_Name
    {
        get
        {
        return studentname;
        }
        set
        {
        studentname = value;
        }
    }

    public string Student_Add
    {
        get
        {
            return studentadd;
        }
        set
        {
            studentadd = value;
        }
    }



    public Student(int studentid, string studentname, string studentadd)
    {
        this.studentname = studentname;
        this.studentadd = studentadd;
        this.studentid = studentid;
    }
}
