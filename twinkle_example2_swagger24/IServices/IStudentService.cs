﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using twinkle_example2_swagger24.Models;

namespace twinkle_example2_swagger24.IServices
{
    public interface IStudentService
    {
        //adding definitions,there is no declaration (declaration is in service layer)
        List<Student> Gets();  //display list of studnet
        Student Get(int id);   //to get single student record
        List<Student> Save(Student student);  //saving the student record into list
        List<Student> Delete(int studentId); //delete record



    }
}
