﻿// See https://aka.ms/new-console-template for more information
//ARRAYS

string[] cars = { "bmw", "ford", "volvo", "ford", "madza" };
Console.WriteLine(cars[3]);

//............................................................................................................................
// JAGGED ARRAYS

/*string[][] s= new string [7][];
s[0] = new string[] { "monday", "firstday" };
s[1] = new string[] { "tuesday", "secondday" };
s[2] = new string[] { "wednesday", "thirdday" };
s[3] = new string[] { "thursday", "fourthday" };
s[4] = new string[] { "friday", "fivethday" };
s[5] = new string[] { "saturday", "sixthday" };
s[6] = new string[] { "sunday", "seventhday" };*/
//............................................................................................................................

//LINQ
string[] mystr = { "tk", "pk", "gk", "yk", "ak" };
var result = Array.Find(mystr, mystr => mystr == "pk");
Console.WriteLine(result);
