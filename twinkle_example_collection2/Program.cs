﻿// See https://aka.ms/new-console-template for more information

public class Vehicles
{
    int vehicleid = 0;
    string vehiclename = string.Empty;
    string vehicletype = string.Empty;

    public int V_Id
    {
        get { return vehicleid; }
        set { vehicleid = value; }

    }
    public string Vehicle_Name
    {
        get
        {
            return vehiclename;
        }
        set
        {
            vehiclename = value;
        }
    }

    public string Vehicle_Type
    {
        get
        {
            return vehicletype;
        }
        set
        {
            vehicletype = value;
        }
    }



    public Vehicles(int vehicleid, string vehiclename, string vehicletype)
    {
        this.vehiclename = vehiclename;
        this.vehicletype = vehicletype;
        this.vehicleid = vehicleid;
    }
}
