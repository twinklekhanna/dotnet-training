﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace twinkle_example_swaggerapiproject24.Models
{
    public partial class Table1
    {
        [Required]
        public string Empid { get; set; }
        [Required]
        public string Ename { get; set; }

        [Required]
        public string Dept { get; set; }
    }
}
