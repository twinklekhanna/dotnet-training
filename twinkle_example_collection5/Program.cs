﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
public class Bank
{
    int bankid = 0;
    string bankname = string.Empty;
    string bankbranch = string.Empty;

    public int B_Id
    {
        get { return bankid; }
        set { bankid = value; }

    }
    public string bank_Name
    {
        get
        {
            return bankname;
        }
        set
        {
            bankname = value;
        }
    }

    public string Bank_Branch
    {
        get
        {
            return bankbranch;
        }
        set
        {
            bankbranch = value;
        }
    }



    public Bank(int bankid, string bankname, string bankbranch)
    {
        this.bankname = bankname;
        this.bankbranch = bankbranch;
        this.bankid = bankid;
    }
}