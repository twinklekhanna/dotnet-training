﻿// See https://aka.ms/new-console-template for more information


public class SomeClass1
{
    public void GenericMethod<T>(T Param1, T Param2)
    {
        Console.WriteLine($"Parameter 1:{Param1} : Parameter 2: {Param2}");
    }
}
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Generics Method Example1");
        SomeClass1 s = new SomeClass1();
        s.GenericMethod<int>(07, 03);
        s.GenericMethod(10.5, 20);
        s.GenericMethod("Twinkle", "khanna");
        Console.ReadLine();
    }
}