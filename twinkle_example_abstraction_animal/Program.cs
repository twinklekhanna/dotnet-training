﻿
// See https://aka.ms/new-console-template for more information


abstract class Animal
{

    // abstract method
    public abstract void makeSound();
}

// inheriting from abstract class
class Dog : Animal
{

    // provide implementation of abstract method
    public override void makeSound()
    {

        Console.WriteLine("Bark Bark");

    }
}

internal class Program
{
    static void Main(string[] args)
    {
        // create an object of Dog class
        Dog obj = new Dog();
        obj.makeSound();

        Console.ReadLine();
    }
}