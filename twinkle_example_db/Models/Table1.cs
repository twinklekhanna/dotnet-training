﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace twinkle_example_db.Models
{
    public partial class Table1
    {
        [Required]
        public string Empid { get; set; }
        [Required]
        public string Dept { get; set; }
        [Required]
        public string Empname { get; set; }
    }
}
