﻿// See https://aka.ms/new-console-template for more information

public class Books
{
    int bookid = 0;
    string bookname = string.Empty;
    string bookprice = string.Empty;

    public int B_Id
    {
        get { return bookid; }
        set { bookid = value; }

    }
    public string Book_Name
    {
        get
        {
            return bookname;
        }
        set
        {
            bookname = value;
        }
    }

    public string Book_price
    {
        get
        {
            return bookprice;
        }
        set
        {
            bookprice = value;
        }
    }



    public Books(int bookid, string bookname, string bookprice)
    {
        this.bookname = bookname;
        this.bookprice = bookprice;
        this.bookid = bookid;
    }
}