﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using twinkle_example2_swagger24.IServices;
using twinkle_example2_swagger24.Models;

namespace twinkle_example2_swagger24.Services
{
    public class StudentService : IStudentService
    {

        List<Student> _students = new List<Student>();
        public StudentService()
        {
            for (int i = 1; i < 9; i++)
            {
                _students.Add(new Student()
                {
                    StudentId = i,
                    Name = "Stu" + i,
                    Roll = "100" + i
                });
            }
        }
        public List<Student> Delete(int studentId)
        {
            _students.RemoveAll(x => x.StudentId == studentId);
            return _students;

        }

        public Student Get(int id)
        {
           return _students.SingleOrDefault(x => x.StudentId == id);
            
        }

        public List<Student> Gets()
        {
            return _students;
        }

        public List<Student> Save(Student student)
        {
            _students.Add(student);
            return _students;
        }
    }
}
