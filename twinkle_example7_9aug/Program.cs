﻿using System;

namespace twinkle_example7_9aug
{
    class Program
    {
        static void Main(string[] args)
        {
            string n;
            Console.WriteLine("Enter your full name");
            n = Console.ReadLine();

            int length = n.Length;
            string leftn = n.Substring(length-5);
            Console.WriteLine(leftn);

        }
    }
}
