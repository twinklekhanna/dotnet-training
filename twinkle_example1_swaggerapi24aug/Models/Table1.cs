﻿using System;
using System.Collections.Generic;

namespace twinkle_example1_swaggerapi24aug.Models
{
    public partial class Table1
    {
        public string Empid { get; set; }
        public string Ename { get; set; }
        public string Dept { get; set; }
    }
}
