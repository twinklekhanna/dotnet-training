﻿using System;

namespace twinkle_example2_9aug
{
    class Program
    {
        static void Main(string[] args)
        {
           
            // Console.WriteLine("Hello World!");
            // lets discuss what variables are..

            int FirstVal;
            int SecondVal;

            Console.WriteLine("Please enter first number");
            FirstVal = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Please enter second number");
            SecondVal = Convert.ToInt32(Console.ReadLine());
            
            //Multiplication

            int result = FirstVal * SecondVal;
            Console.WriteLine(result);

        }
    }
}
